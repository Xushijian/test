#include <stdio.h>

static void docal24(void);
static int docal(int *srcbuf, int num);

int main(void)
{


	while (1)
	{
		docal24();
		docal24();
		docal24();
	}

		docal24();
	return 0;
}

static void docal24(void)
{
	int buf[4];
	printf("Please input 4 num:\n");
	scanf_s("%d %d %d %d", buf, buf + 1, buf + 2, buf + 3);
	docal(buf, 4);
}

static int docal(int *srcbuf, int num)
{
	int i1, i2, i3;
	int buf[4];
	int a, b;

	docal24();

	if ((num == 1) && (srcbuf[0] == 24))
	{
		printf("*** result：24 ***\n");
		return 1;
	}

	for (i1 = 0; i1 < num; i1++)
	{
		for (i2 = 0; i2 < num; i2++)
		{
			if (i2 == i1) continue;

			for (i3 = 0; i3 < 4; i3++)	// 四则运算
			{
				int ptr = 0;
				int flag_canNotCal = 0;
				switch (i3)
				{
				case 0:	// +
					buf[ptr++] = srcbuf[i1] + srcbuf[i2];
					break;
				case 1: // -
					if (srcbuf[i1] < srcbuf[i2])	// 避免负数
					{
						flag_canNotCal = 1;
						break;
					}

					buf[ptr++] = srcbuf[i1] - srcbuf[i2];
					break;
				case 2:	// *
					buf[ptr++] = srcbuf[i1] * srcbuf[i2];
					break;
				default: // /
					if (srcbuf[i2] == 0)
					{
						flag_canNotCal = 1;
						break;
					}
					if (srcbuf[i1] % srcbuf[i2] == 0)	// 避免无法整除
						buf[ptr++] = srcbuf[i1] / srcbuf[i2];
					else
					{
						flag_canNotCal = 1;
						break;
					}
					break;
				}

				if (flag_canNotCal) continue;

				for (int j = 0; j < num; j++)	/*< 减少一个数 */
				{
					if ((j != i1) && (j != i2))
						buf[ptr++] = srcbuf[j];
				}
				if (docal(buf, ptr) == 1)
				{
					printf("%d", srcbuf[i1]);
					switch (i3)
					{
					case 0:
						printf(" +");
						break;
					case 1:
						printf(" -");
						break;
					case 2:
						printf(" *");
						break;
					default:
						printf(" /");
						break;
					}
					printf(" %d = %d\n", srcbuf[i2], buf[0]);
					return 1;
				}
			}

		}
	}

	return 0;
}
